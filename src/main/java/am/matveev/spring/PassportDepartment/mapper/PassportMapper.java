package am.matveev.spring.PassportDepartment.mapper;

import am.matveev.spring.PassportDepartment.dto.PassportDTO;
import am.matveev.spring.PassportDepartment.entity.PassportEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PassportMapper{

    PassportDTO toDTO(PassportEntity passport);
    PassportEntity toEntity(PassportDTO passportDTO);
}
