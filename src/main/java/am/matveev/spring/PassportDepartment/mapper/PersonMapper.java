package am.matveev.spring.PassportDepartment.mapper;

import am.matveev.spring.PassportDepartment.dto.PersonDTO;
import am.matveev.spring.PassportDepartment.entity.PersonEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface PersonMapper{

    PersonDTO toDTO(PersonEntity person);
    PersonEntity toEntity(PersonDTO personDTO);
    @Mapping(source = "passport", target = "passportDTO")
    PersonDTO toDTOWithPassport(PersonEntity person);
}
