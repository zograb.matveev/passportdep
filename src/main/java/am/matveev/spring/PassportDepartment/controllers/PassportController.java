package am.matveev.spring.PassportDepartment.controllers;

import am.matveev.spring.PassportDepartment.dto.PassportDTO;
import am.matveev.spring.PassportDepartment.entity.PassportEntity;
import am.matveev.spring.PassportDepartment.services.PassportService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/passport")
@RestController
@RequiredArgsConstructor
public class PassportController{

    private final PassportService passportService;

    @GetMapping()
    public List<PassportDTO> findAll(){
        return passportService.findAll();
    }

    @GetMapping("/{id}")
    public PassportDTO findOne(@PathVariable long id){
        return passportService.findOne(id);
    }

    @PostMapping()
    public PassportDTO create(@RequestBody @Valid PassportDTO passportDTO){
        return passportService.create(passportDTO);
    }

    @PutMapping("/{id}")
    public PassportDTO update(@PathVariable long id, @RequestBody @Valid PassportDTO passportDTO){
        return passportService.update(id,passportDTO);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id){
        passportService.delete(id);
    }
}
