package am.matveev.spring.PassportDepartment.controllers;

import am.matveev.spring.PassportDepartment.dto.PassportDTO;
import am.matveev.spring.PassportDepartment.dto.PersonDTO;
import am.matveev.spring.PassportDepartment.services.PersonService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/person")
@RequiredArgsConstructor
public class PersonController{

    private final PersonService personService;

    @GetMapping()
    public List<PersonDTO> findAll(){
        return personService.findAll();
    }

    @GetMapping("/{id}")
    public PersonDTO findOne(@PathVariable long id){
        return personService.findOne(id);
    }

    @PostMapping()
    public PersonDTO create(@RequestBody PersonDTO personDTO){
        return personService.create(personDTO);
    }

    @PutMapping("/{id}")
    public PersonDTO update(@PathVariable long id,@RequestBody PersonDTO personDTO){
        return personService.update(id,personDTO);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id){
        personService.delete(id);
    }

    @PostMapping("/{personId}/addPassport")
    public ResponseEntity<PersonDTO> addPassportToPerson(
            @PathVariable long personId,
            @RequestBody PassportDTO passportDTO
    ) {
        PersonDTO updatedPerson = personService.addPassportToPerson(personId, passportDTO);
        return ResponseEntity.ok(updatedPerson);
    }

}
