package am.matveev.spring.PassportDepartment.services;

import am.matveev.spring.PassportDepartment.dto.PassportDTO;
import am.matveev.spring.PassportDepartment.entity.PassportEntity;
import am.matveev.spring.PassportDepartment.exceptions.PassportNotFoundException;
import am.matveev.spring.PassportDepartment.mapper.PassportMapper;
import am.matveev.spring.PassportDepartment.repositories.PassportRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class PassportService{

    private final PassportRepository passportRepository;
    private final PassportMapper passportMapper;

    @Transactional(readOnly = true)
    public List<PassportDTO> findAll(){
        List<PassportEntity> passport = passportRepository.findAll();
        List<PassportDTO> passportDTOS = passport.stream()
                .map(passportMapper::toDTO)
                .collect(Collectors.toList());
        return passportDTOS;
    }

    @Transactional(readOnly = true)
    public PassportDTO findOne(long id){
        PassportEntity passport = passportRepository.findById(id)
        .orElseThrow(PassportNotFoundException ::new);
        return passportMapper.toDTO(passport);
    }

    @Transactional
    public PassportDTO create(PassportDTO passportDTO){
        PassportEntity passport = passportMapper.toEntity(passportDTO);
        passportRepository.save(passport);
        return passportMapper.toDTO(passport);
    }

    @Transactional
    public PassportDTO update(long id,PassportDTO passportDTO){
        PassportEntity passport = passportMapper.toEntity(passportDTO);

        if(passportRepository.findById(id).isEmpty()){
            throw new PassportNotFoundException();
        }
        passport.setId(id);
        passportRepository.save(passport);
        return passportMapper.toDTO(passport);
    }

    @Transactional
    public void delete(long id){
        passportRepository.deleteById(id);
    }
}
