package am.matveev.spring.PassportDepartment.services;

import am.matveev.spring.PassportDepartment.dto.PassportDTO;
import am.matveev.spring.PassportDepartment.dto.PersonDTO;
import am.matveev.spring.PassportDepartment.entity.PassportEntity;
import am.matveev.spring.PassportDepartment.entity.PersonEntity;
import am.matveev.spring.PassportDepartment.exceptions.PersonNotFoundException;
import am.matveev.spring.PassportDepartment.mapper.PassportMapper;
import am.matveev.spring.PassportDepartment.mapper.PersonMapper;
import am.matveev.spring.PassportDepartment.repositories.PersonRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PersonService{

    private final PersonRepository personRepository;
    private final PersonMapper personMapper;

    @Transactional(readOnly = true)
    public List<PersonDTO> findAll(){
        List<PersonEntity> person = personRepository.findAll();
        List<PersonDTO> personDTOS = person.stream()
                .map(personMapper::toDTOWithPassport)
                .collect(Collectors.toList());
        return personDTOS;
    }

    @Transactional(readOnly = true)
    public PersonDTO findOne(long id){
        PersonEntity person = personRepository.findById(id)
                .orElseThrow(PersonNotFoundException::new);
        return personMapper.toDTOWithPassport(person);
    }

    @Transactional
    public PersonDTO create(PersonDTO personDTO){
        PersonEntity person = personMapper.toEntity(personDTO);
        personRepository.save(person);
        return personMapper.toDTO(person);
    }

    @Transactional
    public PersonDTO update(long id, PersonDTO personDTO){
        PersonEntity person = personMapper.toEntity(personDTO);

        if(personRepository.findById(id).isEmpty()){
            throw new PersonNotFoundException();
        }
        person.setId(id);
        personRepository.save(person);
        return personMapper.toDTO(person);
    }

    @Transactional
    public void delete(long id){
        personRepository.deleteById(id);
    }

    @Transactional
    public PersonDTO addPassportToPerson(long personId, PassportDTO passportDTO) {
        PersonEntity person = personRepository.findById(personId)
                .orElseThrow(PersonNotFoundException::new);


        PassportEntity passport = new PassportEntity();
        passport.setPassportNumber(passportDTO.getPassportNumber());
        passport.setIssueDate(passportDTO.getIssueDate());

        passport.setPerson(person);
        person.setPassport(passport);

        personRepository.save(person);

        return personMapper.toDTO(person);
    }
}








