package am.matveev.spring.PassportDepartment.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Entity
@Table(name = "Passport")
@Getter
@Setter
public class PassportEntity{

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "passport_number")
    private String passportNumber;

    @Column(name = "issue_date")
    private Date issueDate;

    @OneToOne
    @JoinColumn(name = "person_id")
    @JsonManagedReference
    private PersonEntity person;
}
