package am.matveev.spring.PassportDepartment.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.LocalDateTime;

@ControllerAdvice
public class GlobalExceptionHandler{

    @ExceptionHandler({PersonNotFoundException.class})
    public ResponseEntity handleException(PersonNotFoundException e){
        ErrorResponse response = new ErrorResponse(
                "Person whit this id wasn't found",
                LocalDateTime.now()
        );
        return new ResponseEntity(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({PassportNotFoundException.class})
    public ResponseEntity handleException(PassportNotFoundException e){
        ErrorResponse response = new ErrorResponse(
                "Pssaport whit this id wasn't found",
                LocalDateTime.now()
        );
        return new ResponseEntity(response,HttpStatus.NOT_FOUND);
    }
}
