package am.matveev.spring.PassportDepartment.dto;


import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.validation.constraints.NotEmpty;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
public class PersonDTO{

    @NotEmpty(message = "Name should not be empty.")
    private String name;

    @NotEmpty(message = "Surname should not be empty.")
    private String surname;

    @NotEmpty(message = "DateOfBirth should not be empty.")
    private Date dateOfBirth;

    private PassportDTO passportDTO;
}
