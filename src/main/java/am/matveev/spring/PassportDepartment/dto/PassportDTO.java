package am.matveev.spring.PassportDepartment.dto;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class PassportDTO{

    @NotEmpty(message = "Passport number should not be empty" )
    private String passportNumber;

    @NotNull(message = "IssueDate should not be null" )
    private Date issueDate;

}
