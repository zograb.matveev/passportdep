package am.matveev.spring.PassportDepartment.repositories;

import am.matveev.spring.PassportDepartment.entity.PersonEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends JpaRepository<PersonEntity,Long>{
}
