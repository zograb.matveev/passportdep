package am.matveev.spring.PassportDepartment.repositories;

import am.matveev.spring.PassportDepartment.entity.PassportEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PassportRepository extends JpaRepository<PassportEntity,Long>{
}
