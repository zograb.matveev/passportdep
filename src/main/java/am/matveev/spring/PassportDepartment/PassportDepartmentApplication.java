package am.matveev.spring.PassportDepartment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PassportDepartmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(PassportDepartmentApplication.class, args);
	}

}
